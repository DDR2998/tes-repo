<?= $this->extend('layout/default') ?>

<?= $this->section('content') ?>
    <!-- Start Content-->
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="card-block">
            <h5 class="m-b-10">Sample Page</h5>
            <p class="text-muted m-b-10">lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
            <ul class="breadcrumb-title b-t-default p-t-10">
                <li class="breadcrumb-item">
                    <a href="<?=site_url('home')?>"> <i class="fa fa-home"></i> </a>
                </li>
                <li class="breadcrumb-item"><a href="#!">Home</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page body start -->
    <div class="page-body">
        <!-- Product list start -->
        <div class="row">

        <?php foreach($product as $item) : ?>
            <div class="col-xl-4 col-md-6 col-sm-6 col-xs-12">
                <div class="card prod-view">
                    <div class="prod-item text-center">
                        <div class="prod-img">
                            <div class="option-hover">
                                <!-- <button type="button" class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-cart-alt f-20"></i>
                                </button> -->
                                <a href="<?=site_url('product/'.$item['slug_product'])?>"  class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                                    <i class="icofont icofont-eye-alt f-20"></i>
                                </a>
                                <!-- <button type="button" class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                                    <i class="icofont icofont-heart-alt f-20"></i>
                                </button> -->
                            </div>
                            <a href="#!" class="hvr-shrink">
                                <img src="<?=base_url('img/product/'.$item['img_product'])?>" class="img-fluid o-hidden" alt="prod1.jpg">
                            </a>
                            <!-- <div class="p-new"><a href="#"> New </a></div> -->
                        </div>
                        <div class="prod-info">
                            <a href="#!" class="txt-muted"><p><b><?=$item['nama_product']?></b></p></a>
                            <div class="m-b-10">
                                <label class="label label-success"><?=$item['kategori']?> <i class="fa fa-shopping-cart"></i></label>
                            </div>
                            <span class="prod-price">RP. <?=$item['harga_product']?> <small class="old-price"><i class="icofont icofont-cur-dollar"></i>RP. <?=$item['harga_product'] + 3000?></small></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
        <!-- Product list end -->
    </div>
    <!-- Page body end -->
</div>
<?= $this->endSection() ?>    
