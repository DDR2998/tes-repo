<?= $this->extend('layout/default') ?>

<?= $this->section('content') ?>
    <!-- Start Content-->
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="card-block">
            <h5 class="m-b-10">Detail Product</h5>
            <ul class="breadcrumb-title b-t-default p-t-10">
                <li class="breadcrumb-item">
                    <a href="<?=site_url('home')?>"> <i class="fa fa-home"></i> </a>
                </li>
                <li class="breadcrumb-item"><a href="<?=site_url('product')?>">Product</a>
                </li>
                <li class="breadcrumb-item"><a href="#!">Product Detail</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Detail Product</h5>
                        <a href="<?=site_url('product')?>" class="btn btn-sm btn-info float-right shadow-sm">Kembali Ke Product</a>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-5 col-xs-12">
                                <div class="port_details_all_img row">
                                    <div class="col-lg-12 m-b-15">
                                        <div id="big_banner">
                                            <div class="port_big_img">
                                            <center>
                                                <img class="img img-fluid" src="/img/product/<?=$product['img_product']?>">                           
                                            </center>
                                            </div>     
                                        </div>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-lg-7 col-xs-12 product-detail" id="product-detail">
                                <div class="row">
                                    <div>
                                     
                                            <div class="col-lg-12">
                                                <span class="txt-muted d-inline-block">Product Code: <a href="#!"> <?=$product['id_product'] ;?> </a> </span>
                                                <span class="f-right">Stok : <a href="#!"> <?=$product['stok_product'] ;?> </a> </span>
                                                <hr>
                                            </div>
                                            <div class="col-lg-12">
                                                <span class="txt-muted d-inline-block"><a href="#!"><?=$product['kategori'] ;?></a></span>
                                            </div>
                                            <div class="col-lg-12">
                                                <h4 class="pro-desc"><?=$product['nama_product'] ?></h4>
                                            </div>
                                            <div class="col-lg-12">
                                                <span class="text-primary product-price">RP <?=$product['harga_product'] ;?></span>
                                                <hr>
                                                <p><?=$product['deskripsi_product']?></p>
                                                <hr>
                                            </div>
                                            <div class="col-lg-12 col-sm-12 mob-product-btn">
                                                <a href="<?=site_url('product/edit/'.$product['slug_product'])?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                <form action="<?=site_url('product/'.$product['id_product'])?>" method="post" class="d-inline">
                                                <?=csrf_field();?>
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?');"><i class="fa fa-trash"></i> Hapus</button>
                                                </form>
                                            </div>
                                            <div class="col-lg-12 col-sm-12 mt-5">
                                                <p>Product Masuk : <?=$product['created_at']?></p>
                                                <p>Product Update : <?=$product['updated_at']?></p>
                                            </div>                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>    
