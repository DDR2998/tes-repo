
<?= $this->extend('layout/default') ?>

<?= $this->section('content') ?>
<div class="page-wrapper">
        <!-- Page-header start -->
    <div class="page-header card">
        <div class="card-block">
            <h5 class="m-b-10">Data Product</h5>
            <ul class="breadcrumb-title b-t-default p-t-10">
                <li class="breadcrumb-item">
                    <a href="<?=site_url('home')?>"> <i class="fa fa-home"></i> </a>
                </li>
                <li class="breadcrumb-item"><a href="#!">Product</a>
                </li>
                <li class="breadcrumb-item"><a href="#!">Data Product</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page-body start -->
    <div class="page-body">
        <!-- DOM/Jquery table start -->
        <div class="card">
            <div class="card-header">
                <h5>List Product</h5>
                <a href="<?=site_url('product/create')?>" class="btn btn-primary btn-sm shadow-sm float-right">Tambah Product</a>                  
                <?php if(session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success background-success mt-4 mb-0">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="icofont icofont-close-line-circled text-white"></i>
                    </button>
                    <strong>Success!</strong> Pesan <code><?=session()->getFlashdata('pesan') ?></code>
                </div>
                <?php endif; ?>
            </div>
            <div class="card-block">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-bordered nowrap">
                        <thead class="bg-info">
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Kategori</th>
                                <th>Harga</th>
                                <th>Stock</th>
                                <th>Gambar</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                        <?php foreach($product as $data) : ?>
                            <tr>
                                <td colspan=" "><?=$no++?></td>
                                <td><?=$data['nama_product'] ;?></td>
                                <td><?=$data['kategori'] ;?></td>
                                <td><?=$data['harga_product'] ;?></td>
                                <td><?=$data['stok_product'] ;?></td>                           
                                <td><center><img src="/img/product/<?=$data['img_product'] ?>" class="gambar"></center></td>                           
                                <td><center>                                   
                                        <a class="btn btn-warning btn-sm" href="<?=site_url('product/'.$data['slug_product'])?>"><i class="fa fa-eye"></i></a>
                                    </center>      
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- DOM/Jquery table end -->
    </div>

</div>
<?= $this->endSection() ?>  



