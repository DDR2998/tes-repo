<?= $this->extend('layout/default') ?>

<?= $this->section('content') ?>
    <!-- Start Content-->
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="card-block">
            <h5 class="m-b-10">Tambah Data Product</h5>
            <ul class="breadcrumb-title b-t-default p-t-10">
                <li class="breadcrumb-item">
                    <a href="<?=site_url('home')?>"> <i class="fa fa-home"></i> </a>
                </li>
                <li class="breadcrumb-item"><a href="<?=site_url('product')?>">Product</a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-header bg-c-lite-green mb-2">
                        <h5 class="mt-2">Tambah Data Product</h5>   
                        <a href="<?=site_url('product')?>" class="btn btn-sm btn-info float-right shadow-sm">Kembali Ke Product</a>              
                    </div>
                    <div class="card-block mt-2">
                        <h4 class="sub-title">Isi Data Product</h4>
                        
                        <form action="<?=site_url('product/save')?>" method="post" enctype="multipart/form-data">
                            <?=csrf_field()?>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Product</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?=($validation->hasError('nama_product'))? 'is-invalid':''?>" name="nama_product" value="<?=old('nama_product')?>" autofocus
                                            placeholder="Nama Product">
                                    <div class="invalid-feedback">
                                        <?=$validation->getError('nama_product') ;?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jenis Product</label>
                                <div class="col-sm-10">
                                    <select name="id_kategori" class="form-control" required>
                                        <option value="<?=(old('id_kategori'))? old('id_kategori') : '' ?>">Pilih Jenis Product</option>
                                        <?php foreach($kategori as $item) : ?>
                                        <option value="<?=$item['id_kategori']?>"><?=$item['kategori']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Harga Product</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="harga_product" value="<?=old('harga_product')?>"
                                            placeholder="Harga Product" required>
                                </div>
                            </div>                      
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Stok Product</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="stok_product" value="<?=old('stok_product')?>"
                                            placeholder="Stok Product" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Deskripsi Product</label>
                                <div class="col-sm-10">
                                    <textarea rows="5" cols="5" class="form-control" name="deskripsi_product"
                                                placeholder="Deskripsi"><?=old('deskripsi_product');?></textarea>
                                </div>
                            </div> 
                            <div class="form-group row">                     
                                <label class="col-sm-2 col-form-label">Upload Gambar</label>
                                <div class="col-sm-2">
                                    <img src="<?=base_url('img/product/default.png') ?>" class="img-thumbnail img-preview">
                                </div>
                                <div class="col-sm-8">
                                        <input type="file" class="form-control custom-file-label  <?=($validation->hasError('img'))? 'is-invalid':''?>" value="<?=old('img');?>"  name="img" id="img" onchange="previewImg()" >
                                        <div class="invalid-feedback">
                                            <?=$validation->getError('img') ;?>
                                        </div> 
                                </div>
                            </div> 
                            <hr>    
                            <div class="mt-4">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-plus-square"></i> Add</button>
                            <button type="reset" class="btn btn-warning float-right btn-sm"><i class="fa fa-minus-square"></i> Batal</button>
                            </div>                    
                        </form>

                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>    
