
<?= $this->extend('layout/default') ?>

<?= $this->section('content') ?>
<div class="page-wrapper">
        <!-- Page-header start -->
    <div class="page-header card">
        <div class="card-block">
            <h5 class="m-b-10">Kategoris</h5>
            <ul class="breadcrumb-title b-t-default p-t-10">
                <li class="breadcrumb-item">
                    <a href="<?=site_url('home')?>"> <i class="fa fa-home"></i> </a>
                </li>
                <li class="breadcrumb-item"><a href="#!">Kategoris</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page-body start -->
    <div class="page-body">
        <!-- DOM/Jquery table start -->
        <div class="card">
            <div class="card-header">
                <h5>List Kategori</h5>
                <a href="<?=site_url('kategori/create')?>" class="btn btn-primary btn-sm shadow-sm float-right">Tambah Kategori</a>                  
                <?php if(session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success background-success mt-4">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="icofont icofont-close-line-circled text-white"></i>
                    </button>
                    <strong>Success!</strong> Pesan <code><?=session()->getFlashdata('pesan') ?></code>
                </div>
                <?php endif; ?>
            </div>
            <div class="card-block">
                <div class="table-responsive dt-responsive">
                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kategori</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1; ?>
                        <?php foreach($kategori as $data) : ?>
                            <tr>
                                <td colspan=" "><?=$no++?></td>
                                <td><?=$data['kategori'] ;?></td>
                                <td><?=$data['created_at'] ;?></td>                                                  
                                <td><center>                                   
                                        <a href="<?=site_url('kategori/edit/'.$data['id_kategori'])?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <form action="<?=site_url('kategori/'.$data['id_kategori'])?>" method="post" class="d-inline">
                                        <?=csrf_field();?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?');"><i class="fa fa-trash"></i> Hapus</button>
                                        </form>
                                    </center>      
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- DOM/Jquery table end -->
    </div>

</div>
<?= $this->endSection() ?>  



