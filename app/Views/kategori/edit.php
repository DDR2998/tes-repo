<?= $this->extend('layout/default') ?>

<?= $this->section('content') ?>
    <!-- Start Content-->
<div class="page-wrapper">
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="card-block">
            <h5 class="m-b-10">Edit Kategori</h5>
            <ul class="breadcrumb-title b-t-default p-t-10">
                <li class="breadcrumb-item">
                    <a href="<?=site_url('home')?>"> <i class="fa fa-home"></i> </a>
                </li>
                <li class="breadcrumb-item"><a href="<?=site_url('kategori')?>">Kategori</a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Edit</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Page-header end -->

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-header bg-c-lite-green mb-2">
                        <h5 class="mt-2">Edit Kategori</h5>   
                        <a href="<?=site_url('kategori')?>" class="btn btn-sm btn-info float-right shadow-sm">Kembali</a>              
                    </div>
                    <div class="card-block mt-2">
                        <h4 class="sub-title">Isi Kategori</h4>
                        
                        <form action="<?=site_url('kategori/update/'.$kategori['id_kategori'])?>" method="post">
                            <?=csrf_field()?>
                            <div class="form-group row">
                            
                                <input type="hidden" value="<?=$kategori['id_kategori']?>" name="id_kategori">
                                <label class="col-sm-2 col-form-label">Kategori</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control <?=($validation->hasError('kategori'))? 'is-invalid':''?>" name="kategori" value="<?=(old('kategori'))? old('kategori') : $kategori['kategori']?>" autofocus
                                            placeholder="kategori">
                                    <div class="invalid-feedback">
                                        <?=$validation->getError('kategori') ;?>
                                    </div>
                                </div>
                            </div>
                            <hr>    
                            <div class="mt-4">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-save"></i> Ubah</button>    
                            </div>                    
                        </form>

                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>    
