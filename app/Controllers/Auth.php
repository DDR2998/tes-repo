<?php

namespace App\Controllers;

class Auth extends BaseController
{


	public function index()
	{
		return redirect()->to(site_url('login'));
	}

	public function login()
	{
		// jika ada session
		if(session('id_user')){
			return redirect()->to(site_url('home'));
		}
		return view('auth/login');
	}

	public function loginproses()
	{
		$post = $this->request->getPost();
		$query = $this->db->table('users')->getWhere(['username' => $post['username']]);
		$user = $query->getRow();

		// dd($user);
		if($user){
			if(password_verify($post['password'], $user->password)){
				$params = ['id_user' => $user->id_user];
				session()->set($params);
				return redirect()->to(site_url('home'));
			}else{
				return redirect()->back()->with('error', 'Password Salah');
			}
		}else{
			return redirect()->back()->with('error', 'Username Salah');
		}
		
	}
	public function logout()
	{
		session()->remove('id_user');
		return redirect()->to(site_url('login'));
	}
}