<?php

namespace App\Controllers;

use App\Models\ProductModel;

class Home extends BaseController
{
	public function __construct()
	{
		$this->productModel = new ProductModel();
	}
	public function index()
	{
		$data = [

			'title' => 'Home | DDR29',
			'product' => $this->productModel->getProduct()
		];
		return view('pages/home', $data);
	}


}