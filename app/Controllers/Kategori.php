<?php

namespace App\Controllers;

use App\Models\KategoriModel;



class Kategori extends BaseController
{
    protected $kategoriModel;
    public function __construct()
    {
        $this->kategoriModel = new KategoriModel();
    }
	public function index()
	{
		$data = [

			'title' => 'Kategori | DDR29',
            'kategori' => $this->kategoriModel->getKategori()
		];
		return view('kategori/index', $data);
	}

    public function create()
    {
        $data = [
            'title' => 'Add Kategori | DDR29',
            'validation' => \Config\Services::validation()
        ];
        return view('kategori/create', $data);
    }

    public function save()
	{
		
		// validasi field input
		if(!$this->validate([
				'kategori' => [
					'rules' => 'required|is_unique[tb_kategori.kategori]',
					'errors' => [
						'required' => '{field} Harus Diisi !!!',
						'is_unique' => '{field} Sudah Ada',
					]
				]
		])){
			return redirect()->to(site_url('kategori/create'))->withInput();
		}

        // ambil inputan
        $kategori = $this->request->getVar('kategori');
		$this->kategoriModel->save([
			'kategori' =>$kategori
		]);
		// membuat notif jika data berhasil  ditambahkan
		session()->setFlashdata('pesan', 'Data Berhasil Ditambahkan');

		// kembalikan ke halaman product
		return redirect()->to(site_url('kategori'));
	}

    public function edit($id_kategori)
	{
		
		$data = [

			'title' => 'Edit Kategori | DDR29',
			'validation' => \Config\Services::validation(),
			'kategori' => $this->kategoriModel->getKategori($id_kategori)
		];
    
		return view('kategori/edit',$data);
	}

    public function update($id_kategori)
	{
		// cek judul
		$kategoriLama = $this->kategoriModel->getKategori($this->request->getVar('id_kategori'));

		if($kategoriLama['kategori'] == $this->request->getVar('kategori')){
			$rule_kategori= 'required';
		}else{
			$rule_kategori= 'required|is_unique[tb_kategori.kategori]';
		}
		// validasi field input
		if(!$this->validate([
				'kategori' => [
					'rules' => $rule_kategori,
					'errors' => [
						'required' => '{field} Harus Diisi !!!',
						'is_unique' => 'Nama kategori Sudah Ada',
					]
				],
		])){
			return redirect()->to(site_url('kategori/edit/'.$this->request->getVar(('id_kategori'))))->withInput();
		}

		// mengambil data dari form edit kategori method post
		$this->kategoriModel->save([
			'id_kategori' => $id_kategori,
			'kategori' =>$this->request->getVar('kategori'),
		]);
		// membuat notif jika data berhasil  ditambahkan
		session()->setFlashdata('pesan', 'Data Berhasil Diubah');
		
		// kembalikan ke halaman kategori
		return redirect()->to(site_url('kategori'));
	}

		public function delete($id_kategori)
	{

		
		$this->kategoriModel->delete($id_kategori);
		$eror = $this->db->error();
		if($eror['code'] != 0){
			session()->setFlashdata('pesan', 'Data tidak dapat dihapus karena sudah berelasi');
		}else{

			session()->setFlashdata('pesan', 'Data berhasil dihapus');
		}
		return redirect()->to(site_url('kategori'));
	}


}