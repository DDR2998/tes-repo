<?php

namespace App\Controllers;

use App\Models\KategoriModel;
use App\Models\ProductModel;
use CodeIgniter\HTTP\Files\UploadedFile;
use Faker\DefaultGenerator;
use phpDocumentor\Reflection\Types\This;

class Product extends BaseController
{
	protected $productModel;
	public function __construct()
	{
		$this->productModel = new ProductModel();
		$this->kategoriModel = new KategoriModel();
		
	}

	public function index()
	{
		// $barang = $this->barangModel->findAll();
		$data = [

			'title' => 'Product | DDR29',
			'product' => $this->productModel->getProduct()
		];


		return view('product/index',$data);
	}
	public function create()
	{

		// menambah session agar input bisa dikembalikan (validation)
		
		$data = [

			'title' => 'Create Product | DDR29',
			'validation' => \Config\Services::validation(),
			'kategori' => $this->kategoriModel->getKategori()
		];


		return view('product/create',$data);
	}


	public function detail($slug)
	{
		$data = [
			'title' => 'Detail | Barang',
			'product' => $this->productModel->getProduct($slug)
		];

		//jika data kosong
		if(empty($data['product'])){
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data tidak ditemukan !!!');
		}

		return view('product/detail', $data);
	}

	public function save()
	{
		
		// validasi field input
		if(!$this->validate([
				'nama_product' => [
					'rules' => 'required|is_unique[tb_product.nama_product]',
					'errors' => [
						'required' => '{field} Harus Diisi !!!',
						'is_unique' => '{field} Nama Product Sudah Ada',
					]
				],
				'img' => [
					'rules' => 'max_size[img,1024]|mime_in[img,image/png,image/jpg,image/jpeg]|is_image[img]',
					'errors' => [
						'max_size' => 'Ukuran gambar terlalu besar',
						'mime_in' => 'Bukan gambar',
						'ext_in' => 'Bukan gambar',
					]
				]
		])){
			// $validation = \Config\Services::validation();
			// return redirect()->to(site_url('product/create'))->withInput()->with('validation', $validation);
			return redirect()->to(site_url('product/create'))->withInput();
		}

		// ambil gambar
		$gambar = $this->request->getFile('img');

		if($gambar->getError() == 4){
			$namaGambar = 'default.png';
		}else{

			// generate nama file
			$namaGambar = $gambar->getRandomName();
			// pindah file ke folder img
			$gambar->move('img/product', $namaGambar);
		}

		// ambil nama file secara langsung
		// $namaGambar = $gambar->getName();

		// membuat slug product dari nama product yang diubah format penulisan nya
		$slug = url_title($this->request->getVar('nama_product'), '-' , true);

		// mengambil data dari form create product method post
		$this->productModel->save([
			'nama_product' =>$this->request->getVar('nama_product'),
			'id_kategori' => $this->request->getVar('id_kategori'),
			'slug_product' => $slug,
			'harga_product' => $this->request->getVar('harga_product'),
			'stok_product' => $this->request->getVar('stok_product'),
			'deskripsi_product' => $this->request->getVar('deskripsi_product'),
			'img_product' => $namaGambar
		]);
		// membuat notif jika data berhasil  ditambahkan
		session()->setFlashdata('pesan', 'Data Berhasil Ditambahkan');

		// kembalikan ke halaman product
		return redirect()->to(site_url('product'));
	}

	public function delete($id_product)
	{
		// cari gambar
		$product = $this->productModel->find($id_product);

		// jika file img default
		if($product['img_product'] != 'default.png'){

			// hapus gambar
			unlink('img/product/'.$product['img_product']);
		}


		$this->productModel->delete($id_product);
		session()->setFlashdata('pesan', 'Data Berhasil Dihapus');
		return redirect()->to(site_url('product'));
	}

	public function edit($slug_product)
	{
		
		$data = [

			'title' => 'Edit Product | DDR29',
			'validation' => \Config\Services::validation(),
			'product' => $this->productModel->getProduct($slug_product),
			'kategori' => $this->kategoriModel->getKategori()
		];


		return view('product/edit',$data);
	}

	public function update($id_product)
	{
		// cek judul
		$productLama = $this->productModel->getProduct($this->request->getVar('slug_product'));

		if($productLama['nama_product'] == $this->request->getVar('nama_product')){
			$rule_nama_product = 'required';
		}else{
			$rule_nama_product = 'required|is_unique[tb_product.nama_product]';
		}
		// validasi field input
		if(!$this->validate([
				'nama_product' => [
					'rules' => $rule_nama_product,
					'errors' => [
						'required' => '{field} Harus Diisi !!!',
						'is_unique' => 'Nama Product Sudah Ada',
					]
				],
				'img' => [
					'rules' => 'max_size[img,1024]|mime_in[img,image/png,image/jpg,image/jpeg]|is_image[img]',
					'errors' => [
						'max_size' => 'Ukuran gambar terlalu besar',
						'mime_in' => 'Bukan gambar',
						'ext_in' => 'Bukan gambar',
					]
				]
		])){
			// $validation = \Config\Services::validation();
			// return redirect()->to(site_url('product/create'))->withInput()->with('validation', $validation);
			return redirect()->to(site_url('product/edit/'.$this->request->getVar(('slug_product'))))->withInput();
		}
		// ambil gambar
		$gambar = $this->request->getFile('img');

		// cek img ->gambar lama
		if($gambar->getError() == 4){
			$namagambar = $this->request->getVar('imgLama');
		}else{
			$namagambar = $gambar->getRandomName();

			// pindah
			$gambar->move('img/product', $namagambar);

			// hapus file lama
			unlink('img/product/'.$this->request->getVar('imgLama'));
		}
		
		// membuat slug product dari nama product yang diubah format penulisan nya
		$slug = url_title($this->request->getVar('nama_product'), '-' , true);

		// mengambil data dari form create product method post
		$this->productModel->save([
			'id_product' => $id_product,
			'nama_product' =>$this->request->getVar('nama_product'),
			'id_kategori' => $this->request->getVar('id_kategori'),
			'slug_product' => $slug,
			'harga_product' => $this->request->getVar('harga_product'),
			'stok_product' => $this->request->getVar('stok_product'),
			'deskripsi_product' => $this->request->getVar('deskripsi_product'),
			'img_product' => $namagambar
		]);
		// membuat notif jika data berhasil  ditambahkan
		session()->setFlashdata('pesan', 'Data Berhasil Diubah');
		
		// kembalikan ke halaman product
		return redirect()->to(site_url('product'));
	}


}
