<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// login
$routes->get('login', 'Auth::login');



$routes->get('/', 'Home::index');


// product
$routes->get('/product', 'Product::index');

$routes->get('/product/create', 'Product::create');

$routes->get('/product/edit/(:segment)', 'Product::edit/$1');

$routes->delete('/product/(:num)', 'Product::delete/$1');

$routes->get('/product/(:any)', 'Product::detail/$1');

// kategori
$routes->get('/kategori', 'Kategori::index');

$routes->get('/kategori/create', 'Kategori::create');

$routes->get('/kategori/edit/(:segment)', 'Kategori::edit/$1');

$routes->delete('/kategori/(:num)', 'Kategori::delete/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
