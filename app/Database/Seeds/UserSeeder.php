<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
	public function run()
	{
		$data = [
			'name' => 'admin',
			'username' => 'admin',
			'password' => password_hash('admin', PASSWORD_BCRYPT),
		];
		$this->db->table('users')->insert($data);
	}
}
