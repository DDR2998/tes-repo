<?php

namespace App\Models;

use CodeIgniter\Model;

class KategoriModel extends Model
{
    protected $table      = 'tb_kategori';
    protected $primaryKey = 'id_kategori';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kategori'
    ];

    public function getKategori($id_kategori = false)
    {
        if($id_kategori == false){

            return $this->findAll();
        }

        return $this->where(['id_kategori' =>$id_kategori])->first();
    }
}