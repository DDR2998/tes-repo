<?php

namespace App\Models;

use CodeIgniter\Model;
class ProductModel extends Model
{
    protected $table      = 'tb_product';
    protected $primaryKey = 'id_product';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'nama_product',
        'id_kategori',
        'slug_product', 
        'harga_product', 
        'stok_product', 
        'img_product',
        'deskripsi_product'
    ];

    public function getProduct($slug = false)
    {
        if($slug == false){
            return $this->join('tb_kategori','tb_kategori.id_kategori = tb_product.id_kategori')->findAll();
        }

        return $this->join('tb_kategori','tb_kategori.id_kategori = tb_product.id_kategori')->where(['slug_product' =>$slug])->first();
    }
}